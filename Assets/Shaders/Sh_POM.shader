﻿Shader "Custom/Sh_POM" 
{
	Properties 
	{
//		_CameraPosition ( "Camera Position", Vector ) = (0.0, 0.0, 0.0, 0.0)
		
//		_MainTex ("Base (RGB)", 2D) = "white" {}
		_HeightTex ("Height (RGB)", 2D) = "white" {}
//		_NormalTex ("Normal (RGB)", 2D) = "white" {}
		
		_HeightScale("HeightScale", Range(0.0, 0.3)) = 0.08
		_Steps("Steps", Range(1, 128)) = 10
        _RefSteps("Refinement Steps", Range(0, 64)) = 5
		
	}
	
	SubShader 
	{
		Pass 
		{
		
			Tags { "Queue" = "Geometry" "IgnoreProjector" = "True" }
			Cull Off
			LOD 200
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#pragma glsl
			#pragma target 3.0
			#pragma exclude_renderers flash
			

			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			sampler2D _HeightTex;
			sampler2D _NormalTex;
			
//			float4 _CameraPosition;
			float _HeightScale;
			int _Steps;
			int _RefSteps;

			struct vIn {
				float4 position : POSITION;
				float3 normal : NORMAL;
				float4 tangent : TANGENT;
				float2 uv : TEXCOORD0;
			};
				
			struct v2f {
				float4  position : SV_POSITION;
				fixed2 uv : TEXCOORD0;
				fixed4 vCol : TEXCOORD1;
				float3 eyeVector : TEXCOORD2;	
			};
			
			
			v2f vert (vIn i) 
			{
				v2f o;
				
				o.position = mul(UNITY_MATRIX_MVP, i.position);
				
				// tangent space, code from UnityCG.cginc
				float3 binormal = cross( i.normal, i.tangent.xyz ) * i.tangent.w;
				float3x3 rotation = float3x3( i.tangent.xyz, binormal, i.normal );
				
				// get object space view vector, code from UnityCG.cginc
				float3 objSpaceCameraPos = mul(_World2Object, float4(_WorldSpaceCameraPos.xyz, 1)).xyz * unity_Scale.w;
				
				// transform view vector to tangent space
				o.eyeVector = mul(rotation, objSpaceCameraPos - i.position.xyz);
				
				
				o.uv = i.uv;
				
				return o;
			}

			float4 frag (v2f i) : COLOR 
			{	
				
				// code from http://www.3dkingdoms.com/CW3D/Doc/CGShaders/DisplacementFrag.cg
				float3 viewVec = normalize(i.eyeVector);
				
				// divide by z, clamped because displacement doesn't work so well at extreme angles
				viewVec /= clamp( viewVec.z, .51, 1 );
				viewVec = float3( -viewVec.x * _HeightScale, -viewVec.y * _HeightScale, -1.0 );
				viewVec /= _Steps;
				
				float3 uv = float3( i.uv.x, i.uv.y, 1.0 );
//				uv.xy -= viewVec.xy * _Steps * 0.5;
				
				float height = 0.0;
				float lastHeight = 1.0;
				
				for ( int i = 0; i <= _Steps; i++ ) {
					
					height = uv.z - tex2D(_HeightTex, uv).x;
					
					if ( height > 0.0 ) {
						uv += viewVec;
						if ( i < _Steps ) {
							lastHeight = height;
						}
					}
					
				}
				
				for ( int j = 0; j < _RefSteps; j++ ) {
					
					height = uv.z - tex2D(_HeightTex, uv).x;
					
					if ( height < 0.0 ) {
						uv -= viewVec / _RefSteps;
						if ( j < _RefSteps ) {
							lastHeight = height;
						}
					}
					
				}
				
//				uv += viewVec * ( height / (height - lastHeight) );
				
				
				return tex2D(_HeightTex, uv).x;
			}
			
			ENDCG
		}
	}
	
}