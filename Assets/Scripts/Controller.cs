﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// rotation from http://forum.unity3d.com/threads/looking-with-the-mouse.109250/

[AddComponentMenu("Camera-Control/Controller")]
public class Controller : MonoBehaviour {
  public enum RotationAxes {
    MouseXAndY = 0,
    MouseX = 1,
    MouseY = 2
  }
  public RotationAxes axes = RotationAxes.MouseXAndY;
  public float sensitivityX = 15F;
  public float sensitivityY = 15F;
  public float minimumX = -360F;
  public float maximumX = 360F;
  public float minimumY = -60F;
  public float maximumY = 60F;
  public float absoluteMoveSpeed = 10F;
  float rotationY = 0F;

  Vector2 mousePos, previousMousePos = Vector2.zero;

	Tour tour;

  // Use this for initialization
  void Start () {
		tour = GetComponent<Tour> ();
    if (rigidbody) {
      rigidbody.freezeRotation = true;
      rigidbody.useGravity = false;
      rigidbody.constraints = RigidbodyConstraints.FreezePositionY;
    }

    UpdateCameraRotation ();
  }
  
  // Update is called once per frame
  void Update () {

		if (Input.GetKeyDown("space")) {
			previousMousePos.x = Input.GetAxis ("Mouse X");
			previousMousePos.y = Input.GetAxis ("Mouse Y");

			tour.tour = false;
		}

		if (Input.GetKey("space")) {
			mousePos.x = Input.GetAxis ("Mouse X") - previousMousePos.x;
			mousePos.y = Input.GetAxis ("Mouse Y") - previousMousePos.y;

			UpdateCameraPosition();
			UpdateCameraRotation();

    }
  }

	void UpdateCameraPosition() {
		float moveSpeed = Time.deltaTime * absoluteMoveSpeed;
		
		transform.position += transform.right * moveSpeed * Input.GetAxis ("Horizontal");
		Vector3 forward = transform.forward;
		//    forward.y = 0;
		//    forward.Normalize ();
		//    forward = Mathf.Abs (forward.y) == 1F ? Vector3.forward : forward;
		transform.position += forward * moveSpeed * Input.GetAxis ("Vertical");
		
		transform.position += transform.up * moveSpeed * Input.GetAxis ("UpDown");
	}

	void UpdateCameraRotation() {
		if (axes == RotationAxes.MouseXAndY) {
			float rotationX = transform.localEulerAngles.y + mousePos.x * sensitivityX;
			
			rotationY += mousePos.y * sensitivityY;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			transform.localEulerAngles = new Vector3 (-rotationY, rotationX, 0);
		} else if (axes == RotationAxes.MouseX)
			transform.Rotate (0, mousePos.x * sensitivityX, 0);
		else {
			rotationY += mousePos.y * sensitivityY;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			transform.localEulerAngles = new Vector3 (-rotationY, transform.localEulerAngles.y, 0);
		}
	}
}