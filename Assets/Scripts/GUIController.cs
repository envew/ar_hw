﻿using UnityEngine;
using System.Collections;
using Extension;

[ExecuteInEditMode]
public class GUIController : MonoBehaviour {

  [SerializeField]
  Texture[]
    pomTextures;

  Material[] pomMaterials;

  Tour t;

  // Use this for initialization
  void Awake () {
    GameObject[] pomObjects = GameObject.FindGameObjectsWithTag (Tags.pomObjects);

    pomMaterials = new Material[pomObjects.Length];

    for (int i = 0; i < pomObjects.Length; i++) {
      pomMaterials [i] = pomObjects [i].renderer.sharedMaterial;
    }

    t = GameObject.Find ("Main Camera").GetComponent<Tour> ();
  }

  public float HeightScaleMin () {
    return 0.0f;
  }
  public float HeightScaleMax () {
    return 0.3f;
  }
  public float HeightScaleCurrent () {
    return pomMaterials [0].GetFloat ("_HeightScale");
  }
  public void HeightScale (float value) {
    for (int i = 0; i < pomMaterials.Length; i++) {
      pomMaterials [i].SetFloat ("_HeightScale", value);
    }
  }

  public float StepsMin () {
    return 1.0f;
  }
  public float StepsMax () {
    return 128.0f;
  }
  public float StepsCurrent () {
    return pomMaterials [0].GetFloat ("_Steps");
  }
  public void Steps (float value) {
    for (int i = 0; i < pomMaterials.Length; i++) {
      pomMaterials [i].SetFloat ("_Steps", value);
    }
  }

  public float RefStepsMin () {
    return 0.0f;
  }
  public float RefStepsMax () {
    return 64.0f;
  }
  public float RefStepsCurrent () {
    return pomMaterials [0].GetFloat ("_RefSteps");
  }
  public void RefSteps (float value) {
    for (int i = 0; i < pomMaterials.Length; i++) {
      pomMaterials [i].SetFloat ("_RefSteps", value);
    }
  }

  public float PomTextureMin () {
    return 0.0f;
  }
  public float PomTextureMax () {
    return (float)pomTextures.Length - 1;
  }
  public float PomTextureCurrent () {
    PomTexture (0.0f);
    return 0.0f;
  }
  public void PomTexture (float value) {
    for (int i = 0; i < pomMaterials.Length; i++) {
      pomMaterials [i].SetTexture ("_HeightTex", pomTextures [(int)value]);
    }
  }

  public float TensionMin () {
    return -1F;
  }
  public float TensionMax () {
    return 1F;
  }
  public float TensionCurrent () {
    return P.TCBTension;
  }
  public void Tension (float value) {
    P.TCBTension = value;
  }

  public float ContinuityMin () {
    return -1F;
  }
  public float ContinuityMax () {
    return 1F;
  }
  public float ContinuityCurrent () {
    return P.TCBContinuity;
  }
  public void Continuity (float value) {
    P.TCBContinuity = value;
  }

  public float BiasMin () {
    return -1F;
  }
  public float BiasMax () {
    return 1F;
  }
  public float BiasCurrent () {
    return P.TCBBias;
  }
  public void Bias (float value) {
    P.TCBBias = value;
  }

  public float ResolutionMin () {
    return 2F;
  }
  public float ResolutionMax () {
    return 1000F;
  }
  public float ResolutionCurrent () {
    return (float)t.Resolution;
  }
  public void Resolution (float value) {
    t.Resolution = (int)value;
  }

  public float MoveSpeedMin () {
    return 1F;
  }
  public float MoveSpeedMax () {
    return 30F;
  }
  public float MoveSpeedCurrent () {
    return t.MoveSpeed;
  }
  public void MoveSpeed (float value) {
    t.MoveSpeed = value;
  }

  public float RotationSpeedMin () {
    return 1F;
  }
  public float RotationSpeedMax () {
    return 30F;
  }
  public float RotationSpeedCurrent () {
    return t.RotationSpeed;
  }
  public void RotationSpeed (float value) {
    t.RotationSpeed = value;
  }
}
