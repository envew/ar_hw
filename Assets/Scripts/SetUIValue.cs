﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[ExecuteInEditMode]
public class SetUIValue : MonoBehaviour {
	
  Text text;

  // Use this for initialization
  void Awake () {
    text = transform.parent.GetChild (1).GetComponent<Text> ();
  }
	
  public void WithFloat (float value) {
    text.text = value.ToString ("F");
  }
}
