﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Reflection;

[ExecuteInEditMode]
public class SetSliderStartValue : MonoBehaviour {

	int SetValueFuncPosition = 0;

	void Start() {

		Slider slider = GetComponent<Slider> ();

		if (slider.onValueChanged.GetPersistentEventCount() >= SetValueFuncPosition) {

			Object target = slider.onValueChanged.GetPersistentTarget(SetValueFuncPosition);

			string parameterName = slider.onValueChanged.GetPersistentMethodName(SetValueFuncPosition);

			// set slider title
			transform.parent.GetChild(0).GetComponent<Text> ().text = parameterName;

			System.Type targetType = target.GetType();
			MethodInfo min = targetType.GetMethod(parameterName + "Min");
			MethodInfo max = targetType.GetMethod(parameterName + "Max");
			MethodInfo current = targetType.GetMethod(parameterName + "Current");

			slider.minValue = (float)min.Invoke(target, null);
			slider.maxValue = (float)max.Invoke(target, null);
			slider.value = (float)current.Invoke(target, null);

			slider.gameObject.GetComponent<SetUIValue>().WithFloat(slider.value);
		}
	}
}
