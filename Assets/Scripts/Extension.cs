﻿using UnityEngine;

// http://forum.unity3d.com/threads/need-help-implementing-quaternion-squad.273931/

namespace Extension {
  public struct P {
    public struct KF {
      public P p;
      public float t;
    }
    public float x, y, z;

    public static float TCBTension = 0F;
    public static float TCBContinuity = 0F;
    public static float TCBBias = 0F;

    public P (P p) {
      x = p.x;
      y = p.y;
      z = p.z;
    }

    public P (Vector3 p) {
      x = p.x;
      y = p.y;
      z = p.z;
    }
    // "Moving Along a Curve with Specified Speed" by David Eberly
    public static P TCBInterpolate (KF p0, KF p1, KF p2, KF p3, float t) {
      float deltaNext = p2.t - p1.t;
      float deltaPrev = p1.t - p0.t;
      float t2 = t * t;
      float t3 = t2 * t;
      
      float h0 = 2 * t3 - 3 * t2 + 1;
      float h1 = -2 * t3 + 3 * t2;
      float h2 = t3 - 2 * t2 + t;
      float h3 = t3 - t2;
      
      Vector3 inTangent = 
        (((1 - TCBTension) * (1 + TCBContinuity) * (1 - TCBBias)) / 2 * (p3.p - p2.p)) +
        (((1 - TCBTension) * (1 - TCBContinuity) * (1 + TCBBias)) / 2 * (p2.p - p1.p));
      Vector3 outTangent = 
        (((1 - TCBTension) * (1 - TCBContinuity) * (1 - TCBBias)) / 2 * (p2.p - p1.p)) +
        (((1 - TCBTension) * (1 + TCBContinuity) * (1 + TCBBias)) / 2 * (p1.p - p0.p));
      inTangent *= (2 * deltaNext) / (deltaPrev + deltaNext);
      outTangent *= (2 * deltaPrev) / (deltaPrev + deltaNext);

      return 
        h0 * p1.p +
        h1 * p2.p +
        h2 * outTangent +
        h3 * inTangent;
    }

    public static implicit operator Vector3 (P p) {
      Vector3 p2 = new Vector3 (p.x, p.y, p.z);
      return p2;
    }
    
    public static implicit operator P (Vector3 p) {
      P p2 = new P (p);
      return p2;
    }

    public static P operator + (P a, P b) {
      return new P ((Vector3)a + (Vector3)b);
    }

    public static P operator + (P a, Vector3 b) {
      return new P ((Vector3)a + b);
    }

    public static P operator - (P a, P b) {
      return a + -1F * b;
    }

    public static P operator * (float a, P b) {
      return new P (a * (Vector3)b);
    }

    public static P operator / (P a, float b) {
      return new P ((Vector3)a / b);
    }
  }

  public struct Q {
    public float x, y, z, w;

    public Vector3 v {
      get { return new Vector3 (x, y, z); }
      set {
        x = value.x;
        y = value.y;
        z = value.z;
      }
    }

    public Q (Q a) {
      x = a.x;
      y = a.y;
      z = a.z;
      w = a.w;
    }

    public Q (float x, float y, float z, float w) {
      this.x = x;
      this.y = y;
      this.z = z;
      this.w = w;
    }
    
    public Q (Quaternion q) {
      x = q.x;
      y = q.y;
      z = q.z;
      w = q.w;
    }

    public void Conjugate () {
      v = -v;
    }

    public Q conjugated {
      get {
        Q q = new Q (this);
        q.Conjugate ();
        return q;
      }
    }

    public void Inverse () {
      float m = Magnitude ();
      Conjugate ();
      this /= m;
    }
    
    public Q inversed {
      get {
        Q q = new Q (this);
        q.Inverse ();
        return q;
      }
    }
    
    public void Normalize () {
      this /= Magnitude ();
    }

    public Q normalized {
      get {
        Q q = new Q (this);
        q.Normalize ();
        return q;
      }
    }
    
    public float SqrMagnitude () {
      return x * x + y * y + z * z + w * w;
    }
    
    public float Magnitude () {
      return Mathf.Sqrt (SqrMagnitude ());
    }
    
    //! returns the logarithm of a quaternion = v*a where q = [cos(a),v*sin(a)]
    public Q Log () {
      float a = Mathf.Acos (w);
      float sina = Mathf.Sin (a);
      Q ret;
      
      ret.w = 0;
      if (sina > 0) {
        ret.x = a * x / sina;
        ret.y = a * y / sina;
        ret.z = a * z / sina;
      } else
        ret.x = ret.y = ret.z = 0;

      return ret;
    }
    
    //! returns e^quaternion = exp(v*a) = [cos(a),vsin(a)]
    public Q Exp () {
      float a = v.magnitude;
      float sina = Mathf.Sin (a);
      float cosa = Mathf.Cos (a);
      Q ret;
      
      ret.w = cosa;
      if (a > 0) {
        ret.x = sina * x / a;
        ret.y = sina * y / a;
        ret.z = sina * z / a;
      } else
        ret.x = ret.y = ret.z = 0;

      return ret;
    }
    
    //! computes the dot product of 2 quaternions
    public static float Dot (Q q1, Q q2) {
      return Vector3.Dot (q1.v, q2.v) + q1.w * q2.w;
    }
    
    //! linear quaternion interpolation
    public static Q Lerp (Q from, Q to, float t) {
      return (from * (1 - t) + to * t).normalized;
    }

    // http://www.mrpt.org/tutorials/programming/maths-and-geometry/slerp-interpolation/
    //! spherical linear interpolation
    public static Q Slerp (Q from, Q to, float t, bool invert = true) {
      Q q;
      float cosTheta = Dot (from, to);
      // if from=to or from=-to then dot = 0 and we can return from
      if (Mathf.Abs (cosTheta) >= 1F)
        return from;

      bool reverseQ = false;
      if (invert && cosTheta < 0) { // Always follow the shortest path
        reverseQ = true;
        cosTheta = -cosTheta;
      }

      float theta = Mathf.Acos (cosTheta);
      float sinTheta = Mathf.Sqrt (1F - (cosTheta * cosTheta));
      // if theta = 180 degrees then result is not fully defined
      // we could rotate around any axis normal to from or to
      if (Mathf.Abs (sinTheta) <= 0.5F)
        return Lerp (from, to, t);

      float A = Mathf.Sin ((1F - t) * theta) / sinTheta;
      float B = Mathf.Sin (t * theta) / sinTheta;
      if (!reverseQ)
        q = A * from + B * to;
      else
        q = A * from - B * to;
      return q;
    }
    
    //! Given 3 quaternions, qn-1,qn and qn+1, calculate a control point to be used in spline interpolation
    static Q SplineControlPoint (Q qnm1, Q qn, Q qnp1) {
      Q qni = qn.inversed;
      return qn * (((qni * qnm1).Log () + (qni * qnp1).Log ()) / -4).Exp ();
    }

    static Q squad (Q q1, Q q2, Q a, Q b, float t) {
      Q c = Slerp (q1, q2, t, false);
      Q d = Slerp (a, b, t, false);
      return Slerp (c, d, 2 * t * (1 - t), false);
    }
    
    public static Q Squad (Q q0, Q q1, Q q2, Q q3, float t) {
      Q a, b;
      a = SplineControlPoint (q0, q1, q2);
      b = SplineControlPoint (q1, q2, q3);
      return squad (q1, q2, a, b, t).normalized;
    }
    
    public static Q operator / (Q a, float b) {
      float iB = 1F / b;
      return a * iB;
    }
    
    public static Q operator * (Q a, Q b) {
      Q r = new Q ();
      r.x = a.y * b.z - a.z * b.y + a.w * b.x + a.x * b.w;
      r.y = a.z * b.x - a.x * b.z + a.w * b.y + a.y * b.w;
      r.z = a.x * b.y - a.y * b.x + a.w * b.z + a.z * b.w;
      r.w = a.w * b.w - Vector3.Dot (a.v, b.v);
      return r;
    }
    
    public static Q operator * (Q a, float b) {
      return new Q (a.x * b, a.y * b, a.z * b, a.w * b);
    }

    public static Q operator * (float a, Q b) {
      return b * a;
    }
    
    public static Q operator + (Q a, Q b) {
      return new Q (a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
    }
    
    public static Q operator - (Q a) {
      return a * -1f;
    }
    
    public static Q operator - (Q a, Q b) {
      return new Q (a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
    }
    
    public static implicit operator Quaternion (Q q) {
      Quaternion q2 = new Quaternion (q.x, q.y, q.z, q.w);
      return q2;
    }
    
    public static implicit operator Q (Quaternion q) {
      Q q2 = new Q (q);
      return q2;
    }
  }
}