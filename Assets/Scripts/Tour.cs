﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Extension;

// tangent vector
using Tangent = UnityEngine.Vector3;

// time t
struct T {
  private float t;
  
  public static implicit operator T (float i) {
    return new T {t=i};
  }
  
  public static implicit operator float (T p) {
    return p.t;
  }
}

// keyframe
struct KF {
  public P p;
  public Q q;
  public T t;
  
  public static implicit operator P.KF (KF kf) {
    return new P.KF {p=kf.p, t=kf.t};
  }
}

static class DebugRay {
  private static List<GameObject> l = new List<GameObject> ();
  public static void Draw (Vector3 pos, Quaternion rot) {
    GameObject go = GameObject.CreatePrimitive (PrimitiveType.Cylinder);
    go.transform.localScale = new Vector3 (0.01F, 0.5F, 0.01F);
    go.transform.position = pos + rot * Vector3.forward * go.transform.localScale.y;
    go.transform.rotation = Quaternion.FromToRotation (Vector3.up, rot * Vector3.forward);
	
		go.renderer.material.color = Color.red;

    l.Add (go);
  }
  public static void Clear () {
    foreach (GameObject go in l) {
      GameObject.Destroy (go);
    }
    l.Clear ();
  }
}

[AddComponentMenu("Camera-Control/Tour")]
public class Tour : MonoBehaviour {
  private int resolution = 30;
  public int Resolution { 
    get { return resolution; } 
    set {
      redraw = true;
      resolution = value;
    }
  }
  private float moveSpeed = 3F;
  public float MoveSpeed {
    get { return moveSpeed; }
    set {
      redraw = true;
      moveSpeed = value;
    }
  }
  private float rotationSpeed = 3F;
  public float RotationSpeed {
    get { return rotationSpeed; }
    set {
      redraw = true;
      rotationSpeed = value;
    }
  }
  public float tension = 0F;
  public float continuity = 0F;
  public float bias = 0F;

  List<Transform> spots;
  int spotIndex = 0;
  int resolutionIndex = 0;
  bool setObject = false;
  bool redraw = false;
  public bool tour = false;

  KF forgotten, prev, next, foreseen, approach;

  void Start () {
    spots = new List<Transform> ();
    forgotten = new KF ();
    prev = new KF ();
  }

  void SetUp () {
    prev = new KF{
      p = spots[0].position,
      q = spots[0].rotation,
      t = 0F
    };
    next = new KF{
      p = spots[1].position,
      q = spots[1].rotation,
      t = 1F
    };
    forgotten = prev;
    foreseen = next;
    forgotten.t = 0F;
    foreseen.t = 2F;
  }
  
  void Walk (out Vector3 pos, out Quaternion rot, int spotIndex, int resolutionIndex) {
    if (spotIndex < spots.Count - 2 && // ensures there is a point to be foreseen
      resolutionIndex == 0) { // ensures this is only processed everytime you move to a new pair of keyframes
      
      foreseen = new KF{
        p = spots[spotIndex+2].position,
        q = spots[spotIndex+2].rotation,
        t =  2F
      };
    }
    
    pos = P.TCBInterpolate (forgotten, prev, next, foreseen, (float)resolutionIndex / resolution);
    rot = Q.Squad (forgotten.q, prev.q, next.q, foreseen.q, (float)resolutionIndex / resolution);
    
    if (resolutionIndex == resolution) { // ensures this is only processed when changing to a new pair of keyframes
      forgotten = prev;
      prev = next;
      next = foreseen;
      forgotten.t = 0F;
      prev.t = 1F;
      next.t = 2F;
      resolutionIndex = 0;
    }
  }
  
  void Update () {
    if (tour) {
      if (Mathf.Approximately (Vector3.Distance (transform.position, approach.p), 0F)) {
        Debug.LogWarning ("reached");
        Vector3 pos;
        Quaternion rot;
        Walk (out pos, out rot, spotIndex, resolutionIndex);
        approach = new KF{
          p = pos,
          q = rot,
          t = 0F
        };
        if (resolutionIndex == resolution) {
          spotIndex++;
          resolutionIndex = 0;
          if (spotIndex == spots.Count - 1) {
            tour = false;

            return;
          }
        } else {
          resolutionIndex++;
        }
      }
      transform.position = Vector3.MoveTowards (transform.position, approach.p, moveSpeed * Time.deltaTime);
      transform.rotation = Quaternion.RotateTowards (transform.rotation, approach.q, rotationSpeed * Time.deltaTime);
      
      return;
    }
    
    if (spots.Count > 0 && Input.GetKeyUp ("p")) {
      tour = true;
      spotIndex = 0;
      SetUp ();
      
      approach = new KF{
        p = transform.position,
        q = transform.rotation,
        t = 0F
      };
    }
    
    if (Input.GetButtonUp ("Fire2")) {
      setObject = true;
      GameObject spot = (GameObject)Instantiate (Resources.Load<GameObject> ("CameraSpot"));
      spot.transform.position = transform.position;
      spot.transform.rotation = transform.rotation;
      spots.Add (spot.transform);

      redraw = true;
    }

    if (tension != P.TCBTension) {
      tension = P.TCBTension;
      redraw = true;
    }
    if (continuity != P.TCBContinuity) {
      continuity = P.TCBContinuity;
      redraw = true;
    }
    if (bias != P.TCBBias) {
      bias = P.TCBBias;
      redraw = true;
    }

    if (redraw && spots.Count > 1) {
      redraw = false;
      DebugRay.Clear ();
      SetUp ();
      for (int i = 0; i < spots.Count - 1; i++) {
        for (int j = 0; j <= resolution; j++) {
          Vector3 pos;
          Quaternion rot;
          Walk (out pos, out rot, i, j);
          DebugRay.Draw (pos, rot);
        }
      }
    }
  }
}
