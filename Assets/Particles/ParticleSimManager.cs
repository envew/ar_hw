﻿using UnityEngine;
using System.Collections;

// code for gpu particle system based on the example project at
// http://scrawkblog.com/2014/02/07/gpu-gems-to-unity-nbody-simulation/

//must be attached to a camera atm because DrawProcedural is used to render the points
//You could move the script of the camera and just pass the buffers to the camera to be rendered in OnPostRender.
[RequireComponent(typeof(Camera))]
public class ParticleSimManager : MonoBehaviour {

	int READ = 0;
	int WRITE = 1;
	const int DEFALUT_SIZE = 65536;

	[SerializeField] Material m_particleMat;
	[SerializeField] ComputeShader m_integrateBodies;
	[SerializeField] int m_numBodies = DEFALUT_SIZE;
	[SerializeField] float m_positionScale = 16.0f;
	[SerializeField] float m_speed = 1.0f;
	[SerializeField] float m_noiseOffsetSpeed = 0.5f;

	[SerializeField] Vector3 m_spawnPos = Vector3.zero;
	[SerializeField] Vector3 m_noiseScale = Vector3.one;
	[SerializeField] float m_spawnSize = 0.4f;

	[SerializeField] float m_minLife = 0.2f;
	[SerializeField] float m_maxLifeDuration = 2.0f;
	


	ComputeBuffer[] m_positions, m_velocities, m_lifes;

	Vector3 m_spawnVel = Vector3.up;

	//p must match the value of NUM_THREADS in the IntegrateBodies shader
	int p = 64;
	int q = 4;


	void Start () {
		m_positions = new ComputeBuffer[2];
		m_velocities = new ComputeBuffer[2];
		m_lifes = new ComputeBuffer[2];

		m_positions [READ] = new ComputeBuffer (m_numBodies, sizeof(float) * 4);
		m_positions [WRITE] = new ComputeBuffer (m_numBodies, sizeof(float) * 4);

		m_velocities [READ] = new ComputeBuffer (m_numBodies, sizeof(float) * 4);
		m_velocities [WRITE] = new ComputeBuffer (m_numBodies, sizeof(float) * 4);

		m_lifes [READ] = new ComputeBuffer (m_numBodies, sizeof(float));
		m_lifes [WRITE] = new ComputeBuffer (m_numBodies, sizeof(float));
		
		m_spawnVel = Vector3.up * 50.0f;

		Config();
	}


	void Config() {
		float scale = m_positionScale * Mathf.Max (1, m_numBodies / DEFALUT_SIZE);

		Vector4[] positions = new Vector4[m_numBodies];
		Vector4[] velocities = new Vector4[m_numBodies];
		float[] lifes = new float[m_numBodies];

		int i = 0;
		while (i < m_numBodies) {
			Vector3 pos = new Vector3 (Random.Range (-1.0f, 1.0f), Random.Range (-1.0f, 1.0f), Random.Range (-1.0f, 1.0f));

			positions [i] = new Vector4(pos.x * scale, pos.y * scale, pos.z * scale, 1.0f);
			velocities [i] = new Vector4(0.0f, 1.0f, 0.0f, 0.0f);
			lifes [i] = 0.0f;

			i++;
    }

    m_positions [READ].SetData (positions);
    m_positions [WRITE].SetData (positions);

    m_velocities [READ].SetData (velocities);
    m_velocities [WRITE].SetData (velocities);

    m_lifes [READ].SetData (lifes);
    m_lifes [WRITE].SetData (lifes);

  }

	void Swap (ComputeBuffer[] buffer) {
//		ComputeBuffer tmp = buffer [READ];
//		buffer [READ] = buffer [WRITE];
//		buffer [WRITE] = tmp;

		READ = 1 - READ;
		WRITE = 1 - WRITE;
	}
	

	void Update () {

		if (Input.GetMouseButtonUp (0)) {
			RaycastHit hit;

			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100.0f)) {
				m_spawnPos = hit.point;
				m_spawnVel = hit.normal * 50.0f;
			}
		}

		m_integrateBodies.SetFloat ("_DeltaTime", Time.deltaTime * m_speed);
		m_integrateBodies.SetFloat ("_NoiseOffset", Time.realtimeSinceStartup * m_noiseOffsetSpeed);
		m_integrateBodies.SetFloat ("_SpawnSize", m_spawnSize);
		m_integrateBodies.SetFloat ("_MinLife", m_minLife);
		m_integrateBodies.SetFloat ("_MaxLifeDuration", m_maxLifeDuration);
		m_integrateBodies.SetInt ("_NumBodies", m_numBodies);
		m_integrateBodies.SetVector ("_SpawnPos", m_spawnPos);
		m_integrateBodies.SetVector ("_SpawnVel", m_spawnVel);
		m_integrateBodies.SetVector ("_NoiseScale", m_noiseScale);
		m_integrateBodies.SetBuffer (0, "_ReadPos", m_positions [READ]);
		m_integrateBodies.SetBuffer (0, "_WritePos", m_positions [WRITE]);
		m_integrateBodies.SetBuffer (0, "_ReadVel", m_velocities [READ]);
		m_integrateBodies.SetBuffer (0, "_WriteVel", m_velocities [WRITE]);
		m_integrateBodies.SetBuffer (0, "_ReadLife", m_lifes [READ]);
		m_integrateBodies.SetBuffer (0, "_WriteLife", m_lifes [WRITE]);

		m_integrateBodies.Dispatch (0, m_numBodies / p, 1, 1);

		Swap (m_positions);
		Swap (m_velocities);
		Swap (m_lifes);
	}

	void OnPostRender () {
		m_particleMat.SetPass (0);
		m_particleMat.SetBuffer ("_Positions", m_positions [READ]);

		Graphics.DrawProcedural (MeshTopology.Points, m_numBodies);
	}

	void OnDestroy () {
		m_positions [READ].Release ();
		m_positions [WRITE].Release ();
		m_velocities [READ].Release ();
		m_velocities [WRITE].Release ();
		m_lifes [READ].Release ();
		m_lifes [WRITE].Release ();
	}
	
}
















