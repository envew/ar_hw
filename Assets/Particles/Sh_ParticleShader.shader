﻿
Shader "Custom/Sh_ParticleShader" 
{

	Properties
	{
		_Sprite("Sprite", 2D) = "white" {}
		_Color("Color", Color) = (0.38,0.26,0.98,1.0)
		_MinSize("Minimum Size", float) = 0.1
		_MaxSize("Maximum Size", float) = 0.1
		_DofPos("Fake Dof Position", float) = 0.5
		_DofWidth("Fake Dof Width", float) = 0.2
	}
	SubShader 
	{
		//uses DrawProcedural so tag doesnt really matter
		Tags { "Queue" = "Transparent" }
	
		Pass 
		{
		
		ZWrite Off 
//		ZTest Always 
		Cull Off 
		Fog { Mode Off }
    	Blend one one

		CGPROGRAM
		#pragma target 5.0

		#pragma vertex vert
		#pragma geometry geom
		#pragma fragment frag

		#include "UnityCG.cginc"

		StructuredBuffer<float4> _Positions;
		StructuredBuffer<float4> _Colors;
		float4 _Color;
		float _MinSize, _MaxSize, _DofPos, _DofWidth;
		sampler2D _Sprite;
		
		float cubicPulse( float c, float w, float x )
		{
		    x = abs(x - c);
		    if( x>w ) return 0.0f;
		    x /= w;
		    return 1.0f - x*x*(3.0f-2.0f*x);
		}
		
		struct v2g 
		{
			float4 pos : SV_POSITION;
			int id : TEXCOORD0;
		};

		v2g vert(uint id : SV_VertexID)
		{
			v2g OUT;
			float3 worldPos = _Positions[id].xyz;
			OUT.pos = mul (UNITY_MATRIX_VP, float4(worldPos,1.0f));
			OUT.id = id;
			return OUT;
		}
		
		struct g2f {
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
			float col : TEXCOORD1;
		};

		[maxvertexcount(4)]
		void geom(point v2g IN[1], inout TriangleStream<g2f> outStream)
		{
			g2f OUT;
			
			OUT.col = cubicPulse(_DofPos, _DofWidth, IN[0].pos.z);
			
			float size = lerp(_MaxSize, _MinSize, OUT.col);
			
			float dx = size;
			float dy = size * _ScreenParams.x / _ScreenParams.y;
			
			OUT.pos = IN[0].pos + float4(-dx, dy,0,0); 
			OUT.uv=float2(0,0); 
			outStream.Append(OUT);
			
			OUT.pos = IN[0].pos + float4( dx, dy,0,0); 
			OUT.uv=float2(1,0); 
			outStream.Append(OUT);
			
			OUT.pos = IN[0].pos + float4(-dx,-dy,0,0); 
			OUT.uv=float2(0,1); 
			outStream.Append(OUT);
			
			OUT.pos = IN[0].pos + float4( dx,-dy,0,0); 
			OUT.uv=float2(1,1); outStream.Append(OUT);
			
			outStream.RestartStrip();
		}

		float4 frag (g2f IN) : COLOR
		{
			float4 col = _Color;
			
			col *= tex2D(_Sprite, IN.uv).r * IN.col;
			return col;
		}

		ENDCG

		}
	}

Fallback Off
}















